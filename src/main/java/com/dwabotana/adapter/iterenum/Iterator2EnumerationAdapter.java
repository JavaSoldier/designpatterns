package com.dwabotana.adapter.iterenum;

import java.util.Enumeration;
import java.util.Iterator;

public class Iterator2EnumerationAdapter implements Enumeration<Object> {
    Iterator<?> iterator;

    public Iterator2EnumerationAdapter(Iterator<?> iterator) {
        this.iterator = iterator;
    }

    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    public Object nextElement() {
        return iterator.next();
    }
}
