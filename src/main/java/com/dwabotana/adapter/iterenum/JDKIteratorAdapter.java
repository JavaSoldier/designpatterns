package com.dwabotana.adapter.iterenum;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

public class JDKIteratorAdapter {
    public static void main(String args[]) {
        Vector<String> v = new Vector<String>(Arrays.asList("enumeration", "to", "iterator"));
        Iterator<String> iterator = v.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
