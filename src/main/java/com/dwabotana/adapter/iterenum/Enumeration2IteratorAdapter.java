package com.dwabotana.adapter.iterenum;

import java.util.Enumeration;
import java.util.Iterator;

public class Enumeration2IteratorAdapter implements Iterator<Object> {
    Enumeration<?> enumeration;

    public Enumeration2IteratorAdapter(Enumeration<?> enumeration) {
        this.enumeration = enumeration;
    }

    public boolean hasNext() {
        return enumeration.hasMoreElements();
    }

    public Object next() {
        return enumeration.nextElement();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
