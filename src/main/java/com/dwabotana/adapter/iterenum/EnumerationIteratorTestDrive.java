package com.dwabotana.adapter.iterenum;

import java.util.*;

public class EnumerationIteratorTestDrive {
    public static void main(String args[]) {
        Vector<String> vector = new Vector<>(Arrays.asList("enumeration", "adapted"));
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList("iterator", "adapted"));

        enumerationAdapter(vector.elements());
        iteratorAdapter(arrayList.iterator());
    }

    public static void enumerationAdapter(Enumeration enumeration) {
        Iterator<?> iterator = new Enumeration2IteratorAdapter(enumeration);
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void iteratorAdapter(Iterator iterator) {
        Enumeration<?> enumeration = new Iterator2EnumerationAdapter(iterator);
        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }
    }


}
