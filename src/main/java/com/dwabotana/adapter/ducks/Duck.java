package com.dwabotana.adapter.ducks;

public interface Duck {
    public void quack();

    public void fly();
}
