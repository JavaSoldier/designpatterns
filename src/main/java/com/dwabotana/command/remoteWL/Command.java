package com.dwabotana.command.remoteWL;

@FunctionalInterface
public interface Command {
    public void execute();
}
