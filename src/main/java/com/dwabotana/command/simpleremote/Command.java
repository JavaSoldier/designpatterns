package com.dwabotana.command.simpleremote;

public interface Command {
    public void execute();
}
