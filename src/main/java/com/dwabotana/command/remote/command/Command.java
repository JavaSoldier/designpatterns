package com.dwabotana.command.remote.command;

public interface Command {
    public void execute();
}
