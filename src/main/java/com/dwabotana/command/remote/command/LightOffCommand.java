package com.dwabotana.command.remote.command;

import com.dwabotana.command.remote.obj.Light;

public class LightOffCommand implements Command {
    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.off();
    }
}
