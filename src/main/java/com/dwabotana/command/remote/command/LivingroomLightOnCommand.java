package com.dwabotana.command.remote.command;

import com.dwabotana.command.remote.obj.Light;

public class LivingroomLightOnCommand implements Command {
    Light light;

    public LivingroomLightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }
}
