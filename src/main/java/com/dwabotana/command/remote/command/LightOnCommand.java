package com.dwabotana.command.remote.command;

import com.dwabotana.command.remote.obj.Light;

public class LightOnCommand implements Command {
    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }
}
