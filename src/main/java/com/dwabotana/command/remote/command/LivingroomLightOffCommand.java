package com.dwabotana.command.remote.command;

import com.dwabotana.command.remote.obj.Light;

public class LivingroomLightOffCommand implements Command {
    Light light;

    public LivingroomLightOffCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.off();
    }
}
