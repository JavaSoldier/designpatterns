package com.dwabotana.command.simpleremoteWL;

@FunctionalInterface
public interface Command {
    public void execute();
}
