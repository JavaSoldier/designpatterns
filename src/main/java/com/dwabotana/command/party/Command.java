package com.dwabotana.command.party;

public interface Command {
    public void execute();

    public void undo();
}
