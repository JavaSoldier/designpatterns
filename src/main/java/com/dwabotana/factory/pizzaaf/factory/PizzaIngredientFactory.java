package com.dwabotana.factory.pizzaaf.factory;

import com.dwabotana.factory.pizzaaf.ingredient.*;

public interface PizzaIngredientFactory {

    Dough createDough();

    Sauce createSauce();

    Cheese createCheese();

    Veggies[] createVeggies();

    Pepperoni createPepperoni();

    Clams createClam();

}
