package com.dwabotana.factory.pizzaaf.ingredient;

public class BlackOlives implements Veggies {

    public String toString() {
        return "Black Olives";
    }
}
