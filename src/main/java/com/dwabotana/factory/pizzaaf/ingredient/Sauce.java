package com.dwabotana.factory.pizzaaf.ingredient;

public interface Sauce {
    public String toString();
}
