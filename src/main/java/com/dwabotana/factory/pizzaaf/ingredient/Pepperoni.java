package com.dwabotana.factory.pizzaaf.ingredient;

public interface Pepperoni {
    public String toString();
}
