package com.dwabotana.factory.pizzaaf.ingredient;

public class SlicedPepperoni implements Pepperoni {

    public String toString() {
        return "Sliced Pepperoni";
    }
}
