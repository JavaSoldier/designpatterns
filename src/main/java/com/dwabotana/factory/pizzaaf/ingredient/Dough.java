package com.dwabotana.factory.pizzaaf.ingredient;

public interface Dough {
    public String toString();
}
