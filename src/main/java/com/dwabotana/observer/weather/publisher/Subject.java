package com.dwabotana.observer.weather.publisher;

import com.dwabotana.observer.weather.subscriber.Observer;

public interface Subject {
    void registerObserver(Observer o);

    void removeObserver(Observer o);

    void notifyObservers();
}
