package com.dwabotana.observer.weather.subscriber;

public interface DisplayElement {
    void display();
}
