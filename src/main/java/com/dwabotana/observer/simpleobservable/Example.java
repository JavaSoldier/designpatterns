package com.dwabotana.observer.simpleobservable;

public class Example {

    public static void main(String[] args) {
        SimpleSubject simpleSubject = new SimpleSubject();

        new SimpleObserver(simpleSubject);

        simpleSubject.setValue(80);
    }
}
