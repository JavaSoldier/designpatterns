package com.dwabotana.observer.simple;

public interface Observer {
    public void update(int value);
}
