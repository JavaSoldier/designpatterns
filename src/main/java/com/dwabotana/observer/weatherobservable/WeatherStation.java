package com.dwabotana.observer.weatherobservable;

import com.dwabotana.observer.weatherobservable.publisher.WeatherData;
import com.dwabotana.observer.weatherobservable.subscriber.CurrentConditionsDisplay;
import com.dwabotana.observer.weatherobservable.subscriber.ForecastDisplay;
import com.dwabotana.observer.weatherobservable.subscriber.HeatIndexDisplay;
import com.dwabotana.observer.weatherobservable.subscriber.StatisticsDisplay;

public class WeatherStation {

    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        weatherData.addObserver(new CurrentConditionsDisplay());
        weatherData.addObserver(new StatisticsDisplay());
        weatherData.addObserver(new ForecastDisplay());
        weatherData.addObserver(new HeatIndexDisplay());

        weatherData.setMeasurements(80, 65, 30.4f);
        System.out.println("----------------->");
        weatherData.setMeasurements(82, 70, 29.2f);
        System.out.println("----------------->");
        weatherData.setMeasurements(78, 90, 29.2f);
    }
}
