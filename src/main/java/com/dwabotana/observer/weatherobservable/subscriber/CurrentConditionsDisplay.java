package com.dwabotana.observer.weatherobservable.subscriber;

import com.dwabotana.observer.weatherobservable.publisher.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private float temperature;
    private float humidity;

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData publisher = (WeatherData) o;
            this.temperature = publisher.getTemperature();
            this.humidity = publisher.getHumidity();
        }
        display();
    }

    public void display() {
        System.out.println("Current conditions: " + temperature
                + "F degrees and " + humidity + "% humidity");
    }
}
