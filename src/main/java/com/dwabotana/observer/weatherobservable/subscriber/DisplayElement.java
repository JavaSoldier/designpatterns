package com.dwabotana.observer.weatherobservable.subscriber;

public interface DisplayElement {
    void display();
}
