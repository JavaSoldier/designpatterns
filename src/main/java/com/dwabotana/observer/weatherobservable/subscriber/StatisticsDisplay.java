package com.dwabotana.observer.weatherobservable.subscriber;

import com.dwabotana.observer.weatherobservable.publisher.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public void update(float temp, float humidity, float pressure) {
        tempSum += temp;
        numReadings++;

        if (temp > maxTemp) {
            maxTemp = temp;
        }

        if (temp < minTemp) {
            minTemp = temp;
        }

        display();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData publisher = (WeatherData) o;
            float newTemperature = publisher.getTemperature();
            tempSum += newTemperature;
            numReadings++;

            if (newTemperature > maxTemp) {
                maxTemp = newTemperature;
            }

            if (newTemperature < minTemp) {
                minTemp = newTemperature;
            }
        }
        display();
    }

    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }
}
