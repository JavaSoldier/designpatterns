package com.dwabotana.strategy.strategyHolders;

import com.dwabotana.strategy.behaivorStrategies.FlyNoWay;
import com.dwabotana.strategy.behaivorStrategies.Quack;

public class ModelDuck extends Duck {
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    public void display() {
        System.out.println("I'm a model duck");
    }
}
