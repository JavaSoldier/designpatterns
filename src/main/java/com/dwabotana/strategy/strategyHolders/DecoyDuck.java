package com.dwabotana.strategy.strategyHolders;

import com.dwabotana.strategy.behaivorStrategies.FlyNoWay;

public class DecoyDuck extends Duck {
    public DecoyDuck() {
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
    }

    public void display() {
        System.out.println("I'm a duck Decoy");
    }
}
