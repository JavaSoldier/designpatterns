package com.dwabotana.strategy.strategyHolders;

import com.dwabotana.strategy.behaivorStrategies.FlyWithWings;
import com.dwabotana.strategy.behaivorStrategies.Quack;

public class MallardDuck extends Duck {

    public MallardDuck() {

        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();

    }

    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
