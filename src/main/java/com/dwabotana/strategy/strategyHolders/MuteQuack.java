package com.dwabotana.strategy.strategyHolders;

import com.dwabotana.strategy.behaivorStrategies.QuackBehavior;

public class MuteQuack implements QuackBehavior {
    public void quack() {
        System.out.println("<< Silence >>");
    }
}
