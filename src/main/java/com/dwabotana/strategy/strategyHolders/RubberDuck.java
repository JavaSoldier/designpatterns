package com.dwabotana.strategy.strategyHolders;

import com.dwabotana.strategy.behaivorStrategies.FlyBehavior;
import com.dwabotana.strategy.behaivorStrategies.FlyNoWay;
import com.dwabotana.strategy.behaivorStrategies.QuackBehavior;
import com.dwabotana.strategy.behaivorStrategies.Squeak;

public class RubberDuck extends Duck {

    public RubberDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Squeak();
    }

    public RubberDuck(FlyBehavior flyBehavior, QuackBehavior quackBehavior) {
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
    }

    public void display() {
        System.out.println("I'm a rubber duckling");
    }
}
