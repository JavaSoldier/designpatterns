package com.dwabotana.strategy;

import com.dwabotana.strategy.behaivorStrategies.FlyNoWay;
import com.dwabotana.strategy.behaivorStrategies.FlyRocketPowered;
import com.dwabotana.strategy.behaivorStrategies.Squeak;
import com.dwabotana.strategy.strategyHolders.*;

public class MiniDuckSimulator {

    public static void main(String[] args) {

        Duck mallard = new MallardDuck();
        Duck rubberDuckling = new RubberDuck(new FlyNoWay(), new Squeak());
        Duck decoy = new DecoyDuck();
        Duck model = new ModelDuck();


        mallard.performQuack();
        rubberDuckling.performQuack();
        decoy.performQuack();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
