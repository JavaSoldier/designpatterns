package com.dwabotana.strategy.challenge;

import com.dwabotana.strategy.challenge.strategies.Email;
import com.dwabotana.strategy.challenge.strategies.Social;
import com.dwabotana.strategy.challenge.strategies.Txt;

import java.util.Scanner;

public class PhotoWithPhone {

    private static String share;
    private static String appName;

    public static void main(String[] args) {
        takeAPhoto();
    }

    private static void setFromInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose camera app - Phone (p) OR CameraPlus (c) ?");
        appName = scanner.next();
        System.out.println("Share with txt (t), email (e), or social media (s)?");
        share = scanner.next();
        scanner.close();
    }

    private static void takeAPhoto() {
        setFromInput();
        BasicCameraApp cameraApp;
        if (appName.equals("c")) {
            cameraApp = new CameraPlusApp();
        } else {
            cameraApp = new PhoneCameraApp();
        }
        switch (share) {
            case ("t"):
                cameraApp.setShareStrategy(new Txt());
                break;
            case ("e"):
                cameraApp.setShareStrategy(new Email());
                break;
            case ("s"):
                cameraApp.setShareStrategy(new Social());
                break;
            default:
                cameraApp.setShareStrategy(new Txt());
        }
        cameraApp.take();
        cameraApp.edit();
        cameraApp.save();
        cameraApp.share();
    }
}
