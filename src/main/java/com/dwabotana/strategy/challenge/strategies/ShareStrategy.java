package com.dwabotana.strategy.challenge.strategies;

@FunctionalInterface
public interface ShareStrategy {
    public void share();
}
