package com.dwabotana.strategy.challenge.strategies;

public class Social implements ShareStrategy {
    public void share() {
        System.out.println("I'm posting the photo on social media");
    }
}
