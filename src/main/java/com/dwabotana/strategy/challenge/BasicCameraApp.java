package com.dwabotana.strategy.challenge;

import com.dwabotana.strategy.challenge.strategies.ShareStrategy;

public abstract class BasicCameraApp {

    ShareStrategy shareStrategy;

    public void setShareStrategy(ShareStrategy shareStrategy) {
        this.shareStrategy = shareStrategy;
    }

    public void share() {
        shareStrategy.share();
    }

    public void take() {//there we can add one more strategy class
        System.out.println("Taking the photo");
    }

    public void save() {//there we can add one more strategy class
        System.out.println("Saving the photo");
    }

    public abstract void edit();
}
