package com.dwabotana.proxy.wiki;

public interface Image {

    void displayImage();
}
